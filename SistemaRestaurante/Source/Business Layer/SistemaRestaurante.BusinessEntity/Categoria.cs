﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SistemaRestaurante.BusinessEntity
{
    public class Categoria
    {
        public int IdCategoria { set; get; }
        public string Nombre { set; get; }
        public string Imagen { set; get; }

    }
}
